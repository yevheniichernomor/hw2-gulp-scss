const button = document.querySelector('.button');
const burgerMenu = document.querySelector('.burger-menu');
const menuLogo = document.querySelector('.menu-icon');
const x = document.querySelector('.x');

function openMenu(){
    burgerMenu.classList.toggle('inactive');
    menuLogo.classList.toggle('inactive');
    x.classList.toggle('inactive');
}

button.addEventListener('click', openMenu);